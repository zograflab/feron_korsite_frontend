if ($("[data-decor-item]").length) {
	var itemsDecor = $("[data-decor-item]");
	$(window).on("scroll", function () {

		itemsDecor.each(function (indx, item) {

			var itemDecor = $(item),
				windowHeight = $(window).outerHeight(true),
				lampTop = item.getBoundingClientRect().top,
				decorCalc;

			if (itemDecor.css("z-index") >= 9) {
				decorCalc = (lampTop - 100) / windowHeight;

			} else {
				decorCalc = 100 / lampTop;
			}

			itemDecor.css({
				filter: "blur(" + decorCalc + "rem)",
				'webkit-filter': "blur(" + decorCalc + "rem)"
			});

		});
	});
}