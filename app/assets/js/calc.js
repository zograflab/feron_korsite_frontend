;"use strict";

var findClosest = function(array, target) {
	var closest = null;
	$.each(array, function(){
		var number = Number(this);
		if (closest == null || Math.abs(number - target) < Math.abs(closest - target)) {
			closest = number;
		}
	});
	return closest;
};

var itemInfo = {
	"title": "ACORN LED 20 D120 5000K",
	"url": "/ru/products/types/indoor-luminaires/damp-proof-luminaires/acorn-led/acorn-led-20-d120-5000k/",
	"photo": "/media/products/luminaires/1490000010_00000144062.jpg",
	"wattage": 21.0,
	"lamp_type": "LED",
	"id": 40165,
	"key": "1490000010",
	"luminous_flux": 2550.0,
	"lamps_number": 1,
	"price": {
		"rub": "4488",
		"eur": "0"
	},
	"usage_coefficient": {
		"80/30/10": {
			"1.25": "60",
			"1.0": "51",
			"2.0": "74",
			"3.0": "84",
			"4.0": "89",
			"5.0": "92",
			"0.8": "44",
			"0.6": "34",
			"2.5": "80",
			"1.5": "66"
		},
		"0/0/0": {
			"1.25": "49",
			"1.0": "41",
			"2.0": "62",
			"3.0": "73",
			"4.0": "77",
			"5.0": "80",
			"0.8": "34",
			"0.6": "26",
			"2.5": "68",
			"1.5": "55"
		},
		"80/50/30": {
			"1.25": "74",
			"1.0": "64",
			"2.0": "90",
			"3.0": "102",
			"4.0": "107",
			"5.0": "111",
			"0.8": "56",
			"0.6": "45",
			"2.5": "96",
			"1.5": "81"
		},
		"30/30/10": {
			"1.25": "57",
			"1.0": "48",
			"2.0": "69",
			"3.0": "79",
			"4.0": "83",
			"5.0": "86",
			"0.8": "42",
			"0.6": "33",
			"2.5": "75",
			"1.5": "62"
		},
		"70/50/20": {
			"1.25": "69",
			"1.0": "61",
			"2.0": "82",
			"3.0": "93",
			"4.0": "97",
			"5.0": "100",
			"0.8": "53",
			"0.6": "43",
			"2.5": "88",
			"1.5": "75"
		},
		"50/50/10": {
			"1.25": "64",
			"1.0": "56",
			"2.0": "76",
			"3.0": "85",
			"4.0": "88",
			"5.0": "91",
			"0.8": "50",
			"0.6": "40",
			"2.5": "81",
			"1.5": "69"
		},
		"50/30/10": {
			"1.25": "58",
			"1.0": "50",
			"2.0": "71",
			"3.0": "81",
			"4.0": "85",
			"5.0": "88",
			"0.8": "43",
			"0.6": "33",
			"2.5": "77",
			"1.5": "64"
		},
		"80/80/30": {
			"1.25": "97",
			"1.0": "89",
			"2.0": "108",
			"3.0": "115",
			"4.0": "119",
			"5.0": "121",
			"0.8": "82",
			"0.6": "72",
			"2.5": "112",
			"1.5": "102"
		}
	},
};


function readyCalc() {
	$('.enter-area_2').html('<h2>Объект, получаемый при выборе айтема</h2><pre>' + print_r(itemInfo)+ '</pre>');
	$(document).on('click', '.js--get-lamp', function(e){
		e.preventDefault();
		alert ('Требуемое количество ламп: ' + getCountLamp());
	});
}

function getCountLamp(){
	var formElements = document.forms.inputData.elements,
		koef_otr = formElements.koef_otr.value,
		koef_zap = +formElements.koef_zap.value,
		light_level = +formElements.light_level.value,
		width = +formElements.width.value,
		length = +formElements.length1.value,

		height = +formElements.height.value,
		space = width * length,
		work_area = +formElements.work_area.value,
		room_index,
		closest_room_index,
		usage_coef,
		fixture_luminous_flux,
		result,
		lamp_reflection = (itemInfo.usage_coefficient[koef_otr.split(' ').join('/')]);

	room_index = space / ( ( height - work_area ) * ( width + length) );

	closest_room_index = findClosest(Object.keys(lamp_reflection), room_index);

	if (closest_room_index % 1 === 0) {
		closest_room_index = closest_room_index.toFixed(1)
	}

	usage_coef = lamp_reflection[closest_room_index]/100;

	if (itemInfo.lamp_type === 'LED') {
		fixture_luminous_flux = itemInfo.luminous_flux;
	} else {
		fixture_luminous_flux = itemInfo.lamps_number*itemInfo.luminous_flux;
	}
	result = Math.ceil( light_level * space * koef_zap / ( fixture_luminous_flux * usage_coef ) );

	return (result);
}

document.addEventListener("DOMContentLoaded", readyCalc);








// функция для представления объектов
// только для отладки
function print_r(arr, level) {
	var print_red_text = "";
	if(!level) level = 0;
	var level_padding = "";
	for(var j=0; j<level+1; j++) level_padding += "    ";
	if(typeof(arr) == 'object') {
		for(var item in arr) {
			var value = arr[item];
			if(typeof(value) == 'object') {
				print_red_text += level_padding + "'" + item + "' :\n";
				print_red_text += print_r(value,level+1);
		}
			else 
				print_red_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
		}
	}

	else  print_red_text = "===>"+arr+"<===("+typeof(arr)+")";
	return print_red_text;
}