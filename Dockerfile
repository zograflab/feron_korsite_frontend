# Ставим node.js v10
FROM node:10

# Создаем рабочую директорию под проект
WORKDIR /app

# Копируем package.json'ы
COPY package*.json ./
COPY gulpfile.js ./ 

# Ставим нужные npm пакеты
RUN npm install

# Монтируем исходники к контейнеру для автосборки и хотрелоада
COPY app ./app

# Собираем исходники
RUN npx gulp build

# Прокидываем порт к серверу
EXPOSE 8080

# Запускаем dev таск с сервером
CMD npx gulp dev